# Maintainer: Thaodan <theodorstormgrade@gmail.com>
# Submitter: Christos Nouskas <nous at archlinux dot us>
# PKGBUILD assembled from kernel26
# Some lines from  kernel26-bfs and kernel26-ck
# Credits to respective maintainers
_major=5
_minor=1
#_patchlevel=0
#_subversion=1
_basekernel=${_major}.${_minor}
_srcname=linux-${_major}.${_minor}
pkgbase=linux-pf
_pfrel=2
_kernelname=-pf
pkgver=${_basekernel}.${_pfrel}
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL2')
url="https://gitlab.com/Arch-Enterprise-packaging/linux-pf-preset-pocket"
source=(
  'linux.preset'			        # standard config files for mkinitcpio ramdisk
  'arch-pf.conf'
  '60-linux.hook'
  '90-linux.hook'
)
backup=("etc/mkinitcpio.d/${pkgbase}.preset" 'boot/loader/entries/arch-pf.conf')
pkgname=linux-pf-preset-pocket
provides=( "linux-pf-preset=$pkgver")
pkgdesc="Linux-pf default preset"
install=linux.install
depends=("linux-pf=$pkgver")

package()
{
  install -D -m644 "${srcdir}"/arch-pf.conf "${pkgdir}"/boot/loader/entries/arch-pf.conf

  # install fallback mkinitcpio.conf file and preset file for kernel
  install -D -m644 "${srcdir}/linux.preset" "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # sed expression for following substitutions
  local _subst="
    s|%PKGBASE%|${pkgbase}|g
    s|%KERNVER%|${_kernver}|g
    s|%EXTRAMODULES%|${_extramodules}|g
  "

  # hack to allow specifying an initially nonexisting install file
  sed "${_subst}" "${startdir}/${install}" > "${startdir}/${install}.pkg"
  true && install=${install}.pkg

  # install mkinitcpio preset file
  #sed "${_subst}" ../linux-pf.preset |
  #  install -Dm644 /dev/stdin "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"
  
  # install pacman hooks
  sed "${_subst}" "${srcdir}"/60-linux.hook |
      install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/60-${pkgbase}.hook"
  sed "${_subst}" "${srcdir}"/90-linux.hook |
      install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/90-${pkgbase}.hook"
  
  # set correct depmod command for install
  #sed \
  #  -e  "s/KERNEL_NAME=.*/KERNEL_NAME=${_kernelname}/" \
  #  -e  "s/KERNEL_VERSION=.*/KERNEL_VERSION=${_kernver}/" \
  #  -i "${startdir}/linux.install"
   sed \
    -e "1s|'linux.*'|'${pkgbase}'|" \
    -e "s|ALL_kver=.*|ALL_kver=\"/boot/vmlinuz-${pkgbase}\"|" \
    -e "s|default_image=.*|default_image=\"/boot/initramfs-${pkgbase}.img\"|" \
    -e "s|fallback_image=.*|fallback_image=\"/boot/initramfs-${pkgbase}-fallback.img\"|" \
    -i "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

}
md5sums=('408a033f1332317f312617704edf9f75'
         'f690c6b389ee6d833db5b8ebbcdcadfe'
         'ce6c81ad1ad1f8b333fd6077d47abdaf'
         'a85bfae59eb537b973c388ffadb281ff')
